#ifndef _LCD_MENU_H_
#define _LCD_MENU_H_

typedef void ( * Action_Pointer ) (int argc, char * argv[] );

typedef struct MenuItem_Struct {
   char * text;
   Action_Pointer action;
   struct MenuItem_Struct * prev, * next, * child, * parent;
} MenuItem;

#endif
