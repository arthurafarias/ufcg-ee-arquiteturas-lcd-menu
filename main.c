#include "main.h"
#include "menu.h"
#include <stdio.h>

#ifndef  CS_LCD

#define  CS_LCD    PIN_B2     // Pino de Controle "CS/EN - CHIP //SELECT" do LCD --------- (pino 6).
#define  RS_LCD    PIN_B1     // Pino de Controle "RS - REGISTER //SELECT" do LCD ----- (pino 4).
#define  D4_LCD    PIN_B3     // Pino do Barramento de Dados "D4 - Dado //4" do LCD --- (pino 11).
#define  D5_LCD    PIN_B4       // Pino do Barramento de Dados "D5 - Dado //5" do LCD --- (pino 12).
#define  D6_LCD    PIN_B5     // Pino do Barramento de Dados "D6 - Dado //6" do LCD --- (pino 13).
#define  D7_LCD    PIN_B6   // Pino do Barramento de Dados "D7 - Dado //7" do LCD --- (pino 14).

#endif

#define lcd_type 2            // 0=5x7, 1=5x10, 2=2 linhas
#define lcd_seg_lin 0x40   // Endere�o da segunda linha na RAM do

// A constante abaixo define a seq��ncia de inicializa��o do LCD.
byte CONST seq_ini_lcd[4] = {0x20 | (lcd_type << 2), 0x0c, 1, 6};

void envia_nibble_lcd( byte dado ){

   output_bit(D4_LCD,bit_test(dado,0));     // Coloca os quatro bits nas saidas.
   output_bit(D5_LCD,bit_test(dado,1));
   output_bit(D6_LCD,bit_test(dado,2));
   output_bit(D7_LCD,bit_test(dado,3));

   output_high(CS_LCD);     // D� um pulso no pino de Enable (CS).
   output_low(CS_LCD);
}


void envia_byte_lcd( boolean endereco, byte dado ){

   output_low(RS_LCD);         //Coloca o pino "RS" em "0".
   output_bit(RS_LCD,endereco); //Configura o pino "RS" dependendo do modo selecionado. 
   delay_us(100);                // Aguarda 100 us.

   output_low(CS_LCD);      // Desabilita o Display.
   envia_nibble_lcd(dado >> 4); // Envia a primeira parte do byte.
   envia_nibble_lcd(dado & 0x0f);   // Envia a segunda parte do byte.
}

void ini_lcd_16x2(){
   byte conta;

   output_low(CS_LCD);
   output_low(D4_LCD);
   output_low(D6_LCD);
   output_low(D7_LCD);
   output_low(RS_LCD);


   output_low(CS_LCD);

   delay_ms(15);

// Envia uma seq��ncia de 3 vezes o valor "0x03" e depois o valor   
// "0x02" para configurar o barramento do LCD no modo de 4 bits.
   for(conta = 1 ;conta <= 3 ; ++conta){
      envia_nibble_lcd(3);
      delay_ms(5);
   }

   envia_nibble_lcd(2);

// Envia uma "string" de inicializa��o do Display.
   for(conta = 0; conta <=3 ; ++conta)   
      envia_byte_lcd(0,seq_ini_lcd[conta]);
}


void posicao_xy_lcd( byte x, byte y){
   byte endereco;

   if(y != 1)
      endereco = lcd_seg_lin;
   else
      endereco = 0;

   endereco += x-1;
   envia_byte_lcd(0,0x80|endereco);
}


void exibe_lcd( char caracter){
   switch (caracter){
      case '\f' :            // Envia um byte de Comando de Limpar (CLEAR) o Display.
         envia_byte_lcd(0,1);
         delay_ms(2);
      break;

      case '\n'   :
         posicao_xy_lcd(1,2);
      break;

      case '\b' :
         envia_byte_lcd(0,10);
      break;

  // Envia um Caracter (ASCII) para o Display.
     default:   
         envia_byte_lcd(1,caracter);
     break;

   }
}

int envia_com(int8 cmd){
   
   int val;
   int8 resp;
   
   spi_write(cmd);
   delay_us(10);
   
   resp = spi_read(0xff);
  
   if (resp == 0xfe)
      val = 1;
   else
      val = 0;
   
   return(val);


}

void ler_adc(int opc){
   
   
   
   if(envia_com(0x01)){
      while (!PIN_D2){
         
   
      }
   }

}

void envia_pwm(){
   
   envia_com(0x01);
   
   while(!PIN_D2){
      

   }
}

void envia_da(){

   envia_com(0x01);
   
   while(!PIN_D2){
   
   }

}

// Declara��o do Menu

extern MenuItem node1, node2, node3, node11, node12, node13;

char node1_text[] = "node 1";
char node2_text[] = "node 2";
char node3_text[] = "node 3";
char node11_text[] = "node 11";
char node12_text[] = "node 12";
char node13_text[] = "node 13";

MenuItem node13 = { node13_text, NULL, &node12, &node11, NULL, &node1 };
MenuItem node12 = { node12_text, NULL, &node11, &node13, NULL, &node1 };
MenuItem node11 = { node11_text, NULL, &node13, &node12, NULL, &node1 };
MenuItem node3 = { node3_text, NULL, &node2, &node1, NULL, NULL };
MenuItem node2 = { node2_text, NULL, &node1, &node3, NULL, NULL };
MenuItem node1 = { node1_text, NULL, &node3, &node2, &node11, NULL };
MenuItem * current_menu_node = &node1;

#define BUTTON_SELECT   0b000000100
#define BUTTON_UP       0b000000001
#define BUTTON_DOWN     0b000000010
#define BUTTON_BACK     0b000001000

void main()
{

   int8 action = 0x0, port_status = 0x00, action_triggered = true, action_displayed = false, key_pressed = false, action_dispatched = true;

   setup_psp(PSP_DISABLED);
   setup_spi(SPI_MASTER|SPI_L_TO_H|SPI_CLK_DIV_16);
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DIV_BY_1,140,1);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);
   ini_lcd_16x2();
   
   while (1) {
      
      port_status = input_d() & 0x0f;
      
      if (port_status) {
         action = port_status;
         key_pressed = true; printf("key pressed\r\n");
      } else if (key_pressed) {
         action_dispatched = true; printf("action dispatched\r\n");
         action_triggered = false; 
         key_pressed = false;
      }
      
      if (action_dispatched && !action_triggered) {
      
         switch (action) {
            case BUTTON_UP: if (current_menu_node->prev != NULL) current_menu_node = current_menu_node->prev; break;
            case BUTTON_DOWN: if (current_menu_node->next != NULL) current_menu_node = current_menu_node->next; break;
            case BUTTON_SELECT:
               if (current_menu_node->action != NULL) current_menu_node->action;
               if (current_menu_node->child != NULL) current_menu_node = current_menu_node->child;
               break;
            case BUTTON_BACK:
               if (current_menu_node->parent != NULL) current_menu_node = current_menu_node->parent;
            default: break;
         }
         
         action_dispatched = false; printf("action triggered\r\n");
         action_triggered = true;
         action_displayed = false;
         
      }
      
      if (!action_displayed) {
         printf(exibe_lcd,"\f>%s", current_menu_node->text);
         printf("action displayed\r\n");
         action_displayed = true;
      }
      
   }
   
}
